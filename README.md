> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# Wyatt Dell

## Assignments from LIS4368 - Advanced Web Applications Developmment

> Documents on Display:

#### Document Files

[LIS4368 A1 ReadMe](a1/README.md "Assignment 1") 

[LIS4368 A2 ReadMe](a2/README.md "Assignment 2")

[LIS4368 A3 ReadMe](a3/README.md "Assignment 3")

[LIS4368 P1 ReadMe](p1/README.md "Project 1")

