> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Developmment

## Wyatt Dell

### Assignment 1 Requirements:

*Three Parts:*

1. Ordered-list items
2. Java/JSP/Servlet Developmment Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial);
* git commands w/ short descriptions
* Bitbucket repo links: a)this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - shows the status of files in the index versus the working directory
3. git add - adds file changes in your working directory to your index
4. git commit - takes all of the changes written in the index, creates a new commit object pointing to it and sets the branch to point to that new commit
5. git push - pushes all the modified local objects to the remote repository and advances its brances
6. git pull - fetches the files from the remote repository and advances its branches
7. git clone - makes a Git repository copy from a remote source

#### Assignment Screenshots:
  
*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


*Screenshot of running http://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat_install.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/wjd13/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/wjd13/myteamquotes/ "My Team Quotes Tutorial")
