> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications Developmment

## Wyatt Dell

### Assignment 2 Requirements:


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
  
The following links should properly display:

http://localhost:9999/hello (displays directory, needs index.html)

http://localhost:9999/hello/index.html  (Can rename "HelloHome.html" to "index.html")

http://localhost:9999/hello/sayhello (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class (changed web.xml file)

http://localhost:9999/hello/querybook.html

http://localhost:9999/hello/sayhi (invokes AnotherHelloServlet)



*Screenshot of query results from http://localhost:9999/hello/querybook.html*:

![Query Results Screenshot](img/query.png)
